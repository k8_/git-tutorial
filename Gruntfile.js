module.exports = function(grunt) {
  'use strict';

  grunt.initConfig({
    bower_concat: {
      all: {
        dest: 'dist/bower.js',
        cssDest: 'dist/bower.css',
        bowerOptions: {
          relative: false
        }
      }
    },
    uglify: {
      all: {
        files: {
          'dist/bower.min.js': ['dist/bower.js']
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-bower-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.registerTask('build',['bower_concat','uglify']);
  grunt.registerTask('default', ['build']);
}