# Git Tutorial
Welcome! Let's get started.

1. Download git: [git-scm.org](http://git-scm.org/download)
	For Windows, make sure to _enable_ Command Line Usage
1. Get a bitbucket account: [bitbucket.org](https://bitbucket.org)
1. Open Terminal (Mac) or Cmd (Windows) and:
1. Type these commands into your command line:
	1. ``mkdir git``
	1. ``cd git``
	1. ``git clone https://k8_@bitbucket.org/k8_/git-tutorial``
1. Who are you? Type these commands into your terminal (switching out your name and email):
	* ``git config --global user.name "{{your-name}}"``
	* ``git config --global user.email {{your-email}}``
1. Open a text editor:
	* Mac: TextEdit, TextMate, [SublimeText](http://www.sublimetext.com/)
	* Windows: NotePad, Notepad++, [SublimeText](http://www.sublimetext.com/)

## [Link to slides](https://docs.google.com/presentation/d/1Sqx8YLpRHt1LFapt0bjlSaRScQOew2av-le1ZQX8tgU/edit?usp=sharing)
